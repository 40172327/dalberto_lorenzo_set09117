import sun.awt.AWTIcon32_security_icon_bw32_png;

import java.util.*;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Created by lorenzodalberto on 10/10/2017.
 */
public class Board {

    public static final int ROWS = 9, COLS = 9;
    //Two dimensional array populated with pieces objects
    private static Piece[][] board = new Piece[COLS][ROWS];

    String[] alphabet = {" ", "A", "B", "C", "D", "E", "F", "G", "H"};
    String[] colNumber = {" ", "1", "2", "3", "4", "5", "6", "7", "8"};

    boolean jumpX = false;
    boolean jumpO = false;

    Stack<Piece[][]> moves = new Stack();
    Stack<Piece[][]> redo = new Stack();
    boolean isUndo = false;

    List<PotentialMove> allowedMoves = new ArrayList<>();


    public void populateBoard() {
        for (int row = 0; row <= ROWS - 1; row++) {
            for (int column = 0; column <= COLS - 1; column++) {

                board[row][column] = new Piece(row, column, " ");
                //fills row 1 to 3 with "X"
                if (row > 0 && row <= 3) {
                    if ((row == 1 || row == 3) && (column % 2 == 0)) {
                        board[row][column] = new Piece(row, column, "x");
                    } else if ((row == 2) && (column % 2 == 1)) {
                        board[row][column] = new Piece(row, column, "x");
                    }
                }
                //fills row 6 to 8 with "O"
                if (row > 5) {
                    if ((row == 6 || row == 8) && (column % 2 == 1)) {
                        board[row][column] = new Piece(row, column, "o");
                    } else if ((row != 6 && row != 8) && (column % 2 == 0)) {
                        board[row][column] = new Piece(row, column, "o");
                    }
                }
                //fills row 4 and 5 with nothing
                if ((ROWS == 4 || ROWS == 5) && COLS > 0) {
                    board[row][column] = new Piece(row, column, " ");
                }
                //fills row 0 with the letter
                if (row == 0) {
                    board[row][column] = new Piece(row, column, alphabet[column]);
                }
                //fills column 0 with numbers
                if (column == 0) {
                    board[row][column] = new Piece(row, column, colNumber[row] + " ");
                }

            }

        }
    }

    public void displayBoard() {
        System.out.println();

        for (int r = 0; r < ROWS; r++) {
            for (int c = 0; c < COLS; c++) {

                // alphabet
                if (r == 0) {
                    System.out.print(board[r][c].symbol + " ");
                }
                // numbers
                if (r != 0 && c == 0) {
                    System.out.print(board[r][c].symbol + "|");
                }
                //x
                if (r > 0 && r < 4 && c > 0) {
                    if (board[r][c] != null) {
                        System.out.print(board[r][c].symbol);
                    } else {
                        System.out.print(" ");
                    }
                    System.out.print("|");
                }
                //o
                if (r > 5 && r <= 8 && c > 0) {
                    if (board[r][c] != null) {
                        System.out.print(board[r][c].symbol);
                    } else {
                        System.out.print(" ");
                    }
                    System.out.print("|");
                }
                //empty spaces
                if ((r == 4 || r == 5) && c > 0) {
                    System.out.print(board[r][c].symbol);
                    System.out.print("|");
                }
            }
            if (r == 0) {
                System.out.println();
            }
            System.out.println("");
        }

    }

    //method displaying the game options
    public void playersOptions() {
        System.out.println();
        System.out.println("Please choose one of the following options");
        System.out.println("1 - Human vs Human");
        System.out.println("2 - Human vs Machine");
        System.out.println("3 - Machine vs Machine");

    }

    public void undo() {
        if (moves.isEmpty()){
            System.out.println("No moves stored, please try later");
            return;
        }

        // still old board before undo
        //Initilise a new temporary of a temporary board
        Piece[][] temp = new Piece[9][9];
        //Cloning the board array by value and not by reference
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                Piece tempPiece = board[i][j];
                temp[i][j] = new Piece(tempPiece.getxCoordinate(), tempPiece.getyCoordinate(), tempPiece.getSymbol());
            }
        }
        redo.push(temp);
        isUndo = true;

        //make the undo
        board = moves.pop();

        displayBoard();


    }

    public void redo() {
        if (redo.isEmpty()){
            System.out.println("No moves stored, please try later");
            return;
        }

        // still old board before redo
        //Initilise a new temporary of a temporary board
        Piece[][] temp = new Piece[9][9];
        //Cloning the board array by value and not by reference
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                Piece tempPiece = board[i][j];
                temp[i][j] = new Piece(tempPiece.getxCoordinate(), tempPiece.getyCoordinate(), tempPiece.getSymbol());
            }
        }
        moves.push(temp);
        board = redo.pop();
        displayBoard();
    }


    public boolean askMoveO() {

        System.out.println("It's O's turn to move");

        String startPoint;
        String endPoint;

        //take user input for the move

        Scanner scanner = new Scanner(System.in);
        do {
            System.out.println("Starting point (A1 to H8, CAPITALS ONLY):");
            System.out.println("Type U to undo");
            startPoint = scanner.next();
        } while (!validInput(startPoint));

        if (startPoint.equalsIgnoreCase("u")) {
            undo();
            return true;
        } else if (startPoint.equalsIgnoreCase("r")) {
            redo();
            return true;
        }

        //empties the list allowedMoves every each move
        allowedMoves.clear();

        //
        String convertedInput = convertToArrayIndex(startPoint);
        checkMove(Character.getNumericValue(convertedInput.charAt(0)), Character.getNumericValue(convertedInput.charAt(1)));

        do {
            System.out.println("Ending point (A1 to H8, CAPITALS ONLY) ");
            endPoint = scanner.next();
        } while (!validInput(endPoint));
        String converteEndInput = convertToArrayIndex(endPoint);

        for (int i = 0; i < allowedMoves.size(); i++) {

            int endRow = Character.getNumericValue(converteEndInput.charAt(0));
            int endCol = Character.getNumericValue(converteEndInput.charAt(1));

            if (endRow == allowedMoves.get(i).getEndRow()) {
                if (endCol == allowedMoves.get(i).getEndColumn()) {
                    makeMove(allowedMoves.get(i));
                    return true;
                }
            }
        }
        //System.out.println(startPoint);
        System.out.println("Move not allowed try again");

        return false;
    }

    public boolean askMoveX() {

        System.out.println("It's X turn to move");

        String startPoint;
        String endPoint;

        //take user input for the move
        Scanner scanner = new Scanner(System.in);
        do {
            System.out.println("Starting point (a1 to h8):");
            startPoint = scanner.next();

        } while (!validInput(startPoint));
        if (startPoint.equalsIgnoreCase("u")) {
            undo();
            return true;
        } else if (startPoint.equalsIgnoreCase("r")) {

            redo();
            return true;

        }

        //empties the list allowedMoves every each move
        allowedMoves.clear();

        //
        String convertedInput = convertToArrayIndex(startPoint);
        checkMove(Character.getNumericValue(convertedInput.charAt(0)), Character.getNumericValue(convertedInput.charAt(1)));
        //if the start point is in the wrong location i wont be asked for the end point
        if (!allowedMoves.isEmpty()) {

            do {
                System.out.println("Ending point (a1 to h8) ");
                endPoint = scanner.next();
            } while (!validInput(endPoint));

            String converteEndInput = convertToArrayIndex(endPoint);

            for (int i = 0; i < allowedMoves.size(); i++) {

                int endRow = Character.getNumericValue(converteEndInput.charAt(0));
                int endCol = Character.getNumericValue(converteEndInput.charAt(1));

                if (endRow == allowedMoves.get(i).getEndRow()) {
                    if (endCol == allowedMoves.get(i).getEndColumn()) {
                        makeMove(allowedMoves.get(i));
                        return true;
                    }
                }
            }
        }

       // System.out.println(startPoint);
        System.out.println("Wrong move try again");
        return false;
    }


    public boolean whoWins() {

        int countX = 0;
        int countO = 0;

        for (int i = 0; i < alphabet.length; i++) {

            for (int e = 0; e < alphabet.length; e++) {
                if (board[i][e].symbol.equalsIgnoreCase("x")) {
                    countX++;
                    if (countX == 0) {
                        System.out.println("O player wins!");
                        return true;

                    }
                } else if (board[i][e].symbol.equalsIgnoreCase("o")) {
                    countO++;
                    if (countO == 0) {
                        System.out.println("X player wins!");
                        return true;
                    }
                }
            }
        }
        System.out.println("Score so far\n" +
                " Xs are: " + countX + "\n" + " " +
                "Os are  " + countO);
        return false;
    }


    public void switchPlayer() {

        System.out.println();
        System.out.println();

        int i = 0;


        while (whoWins() == false) {
            boolean validXMove = false;
            boolean validOMove = false;

            //if statements that determines who plays
            if (i % 2 == 0) {

                validXMove = askMoveX();


            } else if (i % 2 == 1) {


                validOMove = askMoveO();
            }

            if (validXMove || validOMove) {
                if (jumpX == true) {
                    jumpX = false;
                    i = 0;
                } else if (jumpO == true) {
                    jumpO = false;
                    i = 1;
                } else {
                    //valid move
                    i++;
                }
            } else {
                displayBoard();
            }

        }
    }

    //check if it's human or machine turn to play
    public void switchMachinePlayer() {

        int i = 0;

        while (whoWins() == false) {

            //if statements that determines who plays
            if (i % 2 == 0) {
                System.out.println("Human plays first with X");

                askMoveX();

            } else if (i % 2 == 1) {
                System.out.println("Machine play with O");

                machineMove("o");
            }
            if (jumpX == true) {
                jumpX = false;
                i = 0;
            } else if (jumpO == true) {
                jumpO = false;
                i = 1;
            } else {
                //valid move
                i++;
            }
        }

    }

    public void machineVSmachine() {

        int i = 0;

        while (whoWins() == false) {
            try {
                Thread.sleep(4000);
            } catch (Exception e) {
            }
            //if statements that determines who plays
            if (i % 2 == 0) {
                System.out.println("Machine plays first with X");

                machineMove("x");

            } else if (i % 2 == 1) {
                System.out.println("Machine play with O");

                machineMove("o");
            }
            if (jumpX == true) {
                jumpX = false;
                i = 0;
            } else if (jumpO == true) {
                jumpO = false;
                i = 1;
            } else {
                //valid move
                i++;
            }
        }

    }

    private void machineMove(String player) {
        //clear the potential moves before the machine makes the move
        allowedMoves.clear();

        try {
            Thread.sleep(2000);
        } catch (Exception e) {

        }

        //iterate through the board to identify the pieces the machine can use
        for (int starRow = 0; starRow <= ROWS - 1; starRow++) {
            //3 should be 1, changed it to avoid out of bound error
            for (int startColumn = 0; startColumn <= COLS - 1; startColumn++) {

                //all the pieces O are identified here
                if (board[starRow][startColumn].symbol.equalsIgnoreCase(player)) {

                    //check if move is valid, if so adds it to allowed moves
                    checkMove(starRow, startColumn);

                }
            }
        }
        //this will tell us how many valid moves we could do
       // System.out.println(allowedMoves.size());
        //i get a new random index of the lits
        Random random = new Random();

        //if no more moves to be made then the last player making a move wins
        if (allowedMoves.isEmpty()) {
            System.out.println(player + " has no more moves to make. It has been defeated");
            System.exit(0);
        }
        //assign the value from the list index to a variable
        int n = random.nextInt(allowedMoves.size());
        //a random move is made

//        System.out.println(allowedMoves.get(n).getStartRow() + "," + allowedMoves.get(n).getStartColumn() + "," +
//                allowedMoves.get(n).getEndRow() + "," + allowedMoves.get(n).getEndColumn());


        makeMove(allowedMoves.get(n));

    }


    //just a method the returns the valid moves for any piece
    private boolean checkMove(int startRow, int startColumn) {

        boolean foundValid = false;

        String player = board[startRow][startColumn].symbol;

        List<Integer> arryRow = new ArrayList<>();
        List<Integer> arryColumn = new ArrayList<>();
        //if statement determine the directions where to go
        //Kings moves are the combined moves of Xs and Os
        if (player.equals("X") || player.equals("O")) {
            if (startColumn == 8) {

                //combining the Xs and Os moves
                //Os moves
                arryColumn.add(-1);
                arryRow.add(1);
                //Xs moves
                arryColumn.add(-1);
                arryRow.add(-1);

            } else if (startColumn == 1) {
                //Combining the Xs and Os moves
                //Os moves
                arryColumn.add(1);
                arryRow.add(1);
                //X moves
                arryColumn.add(1);
                arryRow.add(-1);
            } else {

                //combine the Xs and Os moves
                //x move
                arryColumn.add(-1);
                arryRow.add(1);

                arryColumn.add(1);
                arryRow.add(1);

                // O moves
                arryColumn.add(-1);
                arryRow.add(-1);
                arryColumn.add(1);
                arryRow.add(-1);
            }
        } else if (player.equalsIgnoreCase("X")) {
            if (startColumn == 8) {

                //going left is the only option if you are at the right side of the board
                arryColumn.add(-1);
                arryRow.add(1);

            } else if (startColumn == 1) {
                //going right is the only option if you are at the left side of the board
                arryColumn.add(1);
                arryRow.add(1);
            } else {

                //left
                arryColumn.add(-1);
                arryRow.add(1);
                //right
                arryColumn.add(1);
                arryRow.add(1);
            }
        } else if (player.equalsIgnoreCase("O")) {

            if (startColumn == 8) {

                //going left is the only option if you are at the right side of the board
                //go left
                arryColumn.add(-1);
                arryRow.add(-1);

            } else if (startColumn == 1) {
                //going right is the only option if you are at the left side of the board
                //go right
                arryColumn.add(1);
                arryRow.add(-1);

            } else {

                //left
                arryColumn.add(-1);
                arryRow.add(-1);

                //right
                arryColumn.add(1);
                arryRow.add(-1);
            }

        } else {
            return false;
        }

        //loop through possible directions to see if there is a move that can be done
        for (int i = 0; i < arryColumn.size(); i++) {

            int endRow = startRow + arryRow.get(i);
            int endColumn = startColumn + arryColumn.get(i);


            if (validBoardIndex(endRow, endColumn)) {


                if (board[endRow][endColumn].symbol.equals(" ")) {

                    foundValid = true;
                    allowedMoves.add(new PotentialMove(startRow, startColumn, endRow, endColumn, null, null));

                    System.out.println("found possible allowed move: " + alphabet[startColumn] + " " + startRow + " Endpoint " + alphabet[endColumn] + " " + endRow);
                } else if (board[endRow][endColumn].symbol.equalsIgnoreCase(getOpponentSymbol(board[startRow][startColumn].symbol))) {


                    int rowToJump = endRow + arryRow.get(i);
                    int columnToJump = endColumn + arryColumn.get(i);

                    if (validBoardIndex(rowToJump, columnToJump)) {


                        if (board[rowToJump][columnToJump].symbol.equals(" ")) {
                            // endRow and endCol is going to be the capture piece since it is occupied by opponent piece
                            allowedMoves.add(new PotentialMove(startRow, startColumn, rowToJump, columnToJump, endRow, endColumn));
                            System.out.println("found possible allowed move: " + alphabet[startColumn] + " " + startRow + " Endpoint " + alphabet[columnToJump] + " " + rowToJump + " while capturing " + alphabet[endColumn] + " " + endRow);

                        }
                    }
                }
            }

        }

        return foundValid;
    }

    //method to convert the alphabet into numbers
    private String convertToArrayIndex(String point) {
        String convertedString = "";
        for (int i = 0; i < alphabet.length; i++) {

            if (point.charAt(0) == alphabet[i].charAt(0)) {
                System.out.println("converted location :" + Character.getNumericValue(point.charAt(1)) + i);
                //potential moves for selected stating point piece
                convertedString += Character.getNumericValue(point.charAt(1));
                convertedString += i;

            }
        }
        System.out.println(point + " is converted to :" + convertedString);
        return convertedString;
    }


    private void makeMove(PotentialMove potentialMove) {

        //Initilise a new temporary board
        Piece[][] temp = new Piece[9][9];
        //Cloning the board array by value and not by reference
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                Piece tempPiece = board[i][j];
                temp[i][j] = new Piece(tempPiece.getxCoordinate(), tempPiece.getyCoordinate(), tempPiece.getSymbol());
            }
        }

        moves.push(temp);

        if ((potentialMove.getEndRow() == 8) || (potentialMove.getEndRow() == 1)) {

            //if we can move then we change the symbols to upper case for the king
            board[potentialMove.getEndRow()][potentialMove.getEndColumn()].symbol = board[potentialMove.getStartRow()][potentialMove.getStartColumn()].symbol.toUpperCase();

        } else {

            //if we can move then we change the symbols
            board[potentialMove.getEndRow()][potentialMove.getEndColumn()].symbol = board[potentialMove.getStartRow()][potentialMove.getStartColumn()].symbol;
        }

        //and we assign the starting cell to an empty one
        board[potentialMove.getStartRow()][potentialMove.getStartColumn()].symbol = " ";
        if (potentialMove.getCapturedRow() != null) {
            board[potentialMove.getCapturedRow()][potentialMove.getCapturedCol()].symbol = " ";
        }

        displayBoard();


    }


    private String getOpponentSymbol(String currentsymbol) {
        if (currentsymbol.equalsIgnoreCase("X")) {
            return "O";
        } else {
            return "X";
        }
    }

    //method checks that user enters a valid index in the board
    private boolean validBoardIndex(int testR, int testC) {

        if ((testR >= 1 && testR <= 8) && (testC >= 1 && testC <= 8)) {
            return true;
        }

        return false;
    }

    private boolean validInput(String point) {

        if (point.equalsIgnoreCase("")) {
            return false;
        }
        if (point.equalsIgnoreCase("R")) {
            return true;
        }
        if (point.equalsIgnoreCase("U")) {
            return true;
        }
        if (point.length() != 2) {
            return false;
        }
        //get a sub array from alphabet
        if (!Stream.of(Arrays.copyOfRange(alphabet, 1, alphabet.length)).anyMatch(x -> x.equalsIgnoreCase(point.substring(0, 1)))) {
            return false;
        }
        if (!Stream.of(Arrays.copyOfRange(colNumber, 1, colNumber.length)).anyMatch(x -> x.equals(point.substring(1)))) {
            return false;
        }

        return true;
    }


}


