import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Board board = new Board();


        board.playersOptions();

        Scanner scanner = new Scanner(System.in);

        int playerOption = scanner.nextInt();

        switch (playerOption) {

            case 1:
                board.populateBoard();
                board.displayBoard();
                board.switchPlayer();
            case 2:
                board.populateBoard();
                board.displayBoard();
                board.switchMachinePlayer();

            case 3:
                board.populateBoard();
                board.displayBoard();
                board.machineVSmachine();

        }
    }
}
