/**
 * Created by lorenzodalberto on 10/10/2017.
 */
public class Piece {

    String symbol;
    int xCoordinate;
    int yCoordinate;

    public Piece(int x, int y, String symbol) {
        xCoordinate = x;
        yCoordinate = y;
        this.symbol = symbol;
    }

    public Piece() {}


    public int getxCoordinate(){
        return xCoordinate;
    }

    public int getyCoordinate(){
        return yCoordinate;
    }

    public void setSymbol(String aSymbol){
        symbol = aSymbol ;
    }
    public String getSymbol(){
        return symbol;
    }




}
