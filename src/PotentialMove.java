/**
 * Created by lorenzodalberto on 27/10/2017.
 */

//Object to store potentail moves
public class PotentialMove {
    
    private int startRow;
    private int startColumn;
    private int endRow;
    private int endColumn;
    private Integer capturedRow;
    private Integer capturedCol;

    public Integer getCapturedRow() {
        return capturedRow;
    }

    public void setCapturedRow(Integer capturedRow) {
        this.capturedRow = capturedRow;
    }

    public Integer getCapturedCol() {
        return capturedCol;
    }

    public void setCapturedCol(Integer capturedCol) {
        this.capturedCol = capturedCol;
    }
    public int getStartRow() {
        return startRow;
    }

    public void setStartRow(int startRow) {
        this.startRow = startRow;
    }

    public int getStartColumn() {
        return startColumn;
    }

    public void setStartColumn(int startColumn) {
        this.startColumn = startColumn;
    }

    public int getEndRow() {
        return endRow;
    }

    public void setEndRow(int endRow) {
        this.endRow = endRow;
    }

    public int getEndColumn() {
        return endColumn;
    }

    public void setEndColumn(int endColumn) {
        this.endColumn = endColumn;
    }

    public PotentialMove(){}
    public PotentialMove(int startRow, int startColumn, int endRow, int endColumn){
        this.startRow=startRow;
        this.startColumn =startColumn;
        this.endRow= endRow;
        this.endColumn = endColumn;
    }

    public PotentialMove(int startRow, int startColumn, int endRow, int endColumn, Integer capturedRow, Integer capturedCol){
        this.startRow=startRow;
        this.startColumn =startColumn;
        this.endRow= endRow;
        this.endColumn = endColumn;
        this.capturedRow = capturedRow;
        this.capturedCol = capturedCol;
    }




    
}
